﻿using Microsoft.AspNetCore.Mvc;
using ProductApp.MVCCore.Models;

namespace ProductApp.MVCCore.Controllers
{
    public class ProductController : Controller
    {
        [HttpGet]
        public ActionResult GetProducts()
        {
            List<Product> productDetails = new List<Product>()
            {
                new Product(){Id=1,Name="IQOO",Description="Neo6",isAvailable=true},
                new Product(){Id=2,Name="RealMe",Description="GT Neo2",isAvailable=true},
                new Product(){Id=3,Name="Xiome",Description="Mi 11T",isAvailable=true}

            };
            return View(productDetails);
        }

    }
}
